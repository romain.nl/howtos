This HOWTO explains how to spawn new test networks for Tezos.
It is in particular aimed at test networks of the Alphanet family such as Delphinet
where the goal is to test a protocol proposal.

This guide assumes that you want to activate the protocol directly, but you can also
activate the previous protocol instead and inject the final protocol for a vote.
In that case, make sure to choose protocol parameters (for the initial protocol)
such that the voting period is short.

# Prepare Branch

Although nodes can download and compile the protocol on-the-fly, it is best if the
node comes with the protocol. And in any case you need the baker, endorser and
accuser binaries for the protocol. It's also good to have the test network
configuration as a built-in multinetwork alias, even if one could also just
distribute `config.json` files instead.

Create a branch which will contain the protocol and the network configuration.
The branch name should end in `-opam` to trigger opam tests.

Note that if your branch is not based on the same commit than the protocol branch,
backporting / rebasing the protocol may be difficult.

## Protocol

Once the protocol is ready in `proto_alpha`, snapshot it using `scripts/snapshot_alpha.sh`.
This creates the `src/proto_XXX_YYYYYYYY` folder with the protocol sources.
Commit it to your branch.

## Link Protocol And Register Mempool Filters

Use script `scripts/link_protocol.sh` to modify the `dune` and `.opam`
files of the node, client and codec to link the protocol, and to
modify `src/bin_node/node_config_command.ml` to add a call to
`Prevalidator_filters.register` for the new protocol.

Commit all of this.

## Update .gitlab-ci.yml

Since we added new packages, you need to run `scripts/update_opam_test.sh`
and `scripts/update_unit_test.sh`.

## Choose Genesis Protocol

The network configuration can override parts of the context.
This feature allows us to reuse genesis protocols and override their activation key.
So there is no need to make a new genesis protocol for each network.
You can use `PtYuensgYBb3G3x1hLLbCmcav8ue8Kyd2khADcL5LsT5R1hcXex` (the genesis
protocol which was used for Carthage and Delphinet, among others).
Note the hash, we'll use it in the network configuration.

## Generate Genesis Block Hash

You need a timestamp and a fresh genesis block hash.
The easiest way to generate those is to use the `scripts/gen-genesis/gen_genesis.ml`
OCaml script. Run this and copy the timestamp and the genesis block hash.
It is best to use a timestamp which is close to the time of activation.
You can do that a few hours before activation though.

## Choose Bootstrap Peers

When someone starts a new node for the first time, this node needs a few initial
peers to connect to. Those are called bootstrap peers. You should decide where
these bootstrap peers will run and note their DNS or IP address (and port, if
you don't use the default port).

## Choose Bootstrap Accounts

The network needs bakers to make the chain move. The initial parameters of the network
will contain a list of active baking accounts with some initial funds for this purpose.
Generate keys for those bootstrap baking accounts. You will need the public key
(`edpk`) for the activation script.

Bootstrap bakers need enough funds to cover deposits before they receive their rewards.
Compute this minimal amount carefully, with some margin. Do not forget about the ramp-up
period. Related: https://gitlab.com/tezos/tezos/-/merge_requests/1390

The list of bootstrap accounts also typically include commitments for the faucet.
You can just reuse the list from the activation script of other test networks
(e.g. `scripts/activate-delphinet.sh`).

It is also convenient to generate an inactive (non-baking) account with a lot of funds.
If bakers run out of funds (due to deposits for instance), you will be able to transfer
funds from this account to the baker accounts. It can also be convenient in case someone
needs a large amount of funds, for which using the faucet may not be convenient.

## Call For Participation

For public test networks, it's best to make a call for participation first,
to allow the community to participate as bootstrap peers and/or bootstrap bakers.
A good place to do that is the #test-networks channel of the baking Slack.

### Example

@channel *Delphinet: Call for Participation*

We plan to start a test network for the protocol proposal Delphi
(see the announcement here: https://forum.tezosagora.org/t/delphi/2150) very soon.
If you want you can participate as a bootstrap peer and/or as a bootstrap baker.
In both cases, this is a commitment that you'll stay as long as the network lives.
This is not necessary to participate in the network: in any case you will be able
to get funds from the faucet.

*To participate as a bootstrap peer*, you'll have to start a node built from the
future version 7.4 which will be released with the Delphinet configuration.
This node will be used by other nodes as one of its initial peers, so it must be reachable
(be careful with NATs in particular) and should run for as long as Delphinet is active.
Send me the DNS or the public IP address of your future node to participate.
Also send me the port if you do not plan to use the default one.

*To participate as a bootstrap baker*, you'll have to start a node and a baker,
also built from the future version 7.4. The baker should run for as long as Delphinet
is active. Send me the public key (starting with `edpk`) of your future baker to participate.

If you wish to participate, please send your bootstrap peer IP addresses
and bootstrap baker public keys before 12:00 CEST tomorrow (Sep. 4th).
Also, please be ready to start your node and baker.
If all goes well, the goal is to have the 7.4 release ready tomorrow afternoon to
launch the test network.

## Generate Activation Key

Generate a key pair. It will be used to activate the protocol.
You will store the public key in the network configuration (to replace the activation
key in the genesis context). The private key will be necessary to activate the protocol.
Do not lose it (otherwise you will not be able to activate the protocol) and do not
share it (otherwise one could create alternative activation blocks and thus split
the network).

## Choose Protocol Parameters

You need the list of parameters for the protocol that you will activate.
A good way to choose those constants is to have a look at
`src/proto_XXX_YYYYYYYY/lib_parameters/default_parameters.ml`.
Start from the values for Mainnet, and tweak constants to make the chain run faster.
See the changes we made for other test networks in
http://tezos.gitlab.io/introduction/test_networks.html.

## Create Network Configuration

Edit file `src/bin_node/node_config_file.ml` to add the configuration for your network.

You will need the following information (see previous paragraphs):
- the network alias, of your choosing (e.g. `delphinet`), that will be given
  to `--network` when configuring the multinetwork node;
- the timestamp of the genesis block;
- the genesis block hash;
- the genesis protocol hash;
- the public key for activation (for `~genesis_parameters`);
- the chain name (usually `TEZOS_<NETWORKNAME>_<TIMESTAMP>`);
- the list of addresses and ports for bootstrap peers.

Commit this into your branch.

### Example

This is the configuration for Delphinet:

    let blockchain_network_delphinet =
      make_blockchain_network
        ~alias:"delphinet"
        {
          time = Time.Protocol.of_notation_exn "2020-09-04T07:08:53Z";
          block =
            Block_hash.of_b58check_exn
              "BLockGenesisGenesisGenesisGenesisGenesis355e8bjkYPv";
          protocol =
            Protocol_hash.of_b58check_exn
              "PtYuensgYBb3G3x1hLLbCmcav8ue8Kyd2khADcL5LsT5R1hcXex";
        }
        ~genesis_parameters:
          {
            context_key = "sandbox_parameter";
            values =
              `O
                [ ( "genesis_pubkey",
                    `String
                      "edpkugeDwmwuwyyD3Q5enapgEYDxZLtEUFFSrvVwXASQMVEqsvTqWu" ) ];
          }
        ~chain_name:"TEZOS_DELPHINET_2020-09-04T07:08:53Z"
        ~sandboxed_chain_name:"SANDBOXED_TEZOS"
        ~default_bootstrap_peers:
          [ "delphinet.tezos.co.il";
            "delphinet.smartpy.io";
            "delphinet.kaml.fr";
            "13.53.41.201" ]

Don't forget to add it to the list of built-in networks:

    let builtin_blockchain_networks_with_tags =
      [ ...;
        (9, blockchain_network_delphinet) ]

The number `9` is the tag for the encoding of this network.
It is mostly unused since we don't transfer configuration as binaries, but you should
try to use a number which was never used before.

## Write Activation Script

The last preparation step is to write a script that will inject the activation block
for the protocol, with all of its parameters. You should start from an existing
script such as `scripts/activate-delphinet.sh` (available in the `v7.4` tagged Git commit).

Review and update everything:
- set the protocol hash to the protocol you want to activate;
- keep the list of `commitments` if you want users to be able
  to use https://faucet.tzalpha.net/;
- update the list of `bootstrap_accounts`: put public keys (starting with `edpk`)
  for bootstrap bakers and public key hashes (starting with `tz1` for instance)
  for non-baker bootstrap accounts;
- update the protocol constants (you may need to add / remove / rename some fields
  since protocols tend to change the set of parameters that they need);
- in the `tezos-client activate protocol` command, change the `key` to the alias
  of the activator key (whose public key was put in the genesis parameters of the
  network configuration and for which you must have the private key);
- possibly add `--base-dir` to this client command (but you probably do not want
  to commit this parameter though).

It is convenient to commit this script in the branch to keep a trace of how
the test network was activated.

# Spawn Test Network

After all the preparation work is done (including waiting for the call for participation
to end) and, if possible, not too long after the genesis timestamp, you can start
your test network.

First, have a few (ideally all) bootstrap peers and bootstrap bakers (and their
endorser / accuser) running with your branch. Check that they effectively
connect to each other.

Then, run the activation script. Your test network should now be moving: blocks
should be being baked.

If all goes well, you can announce the test network on the #test-networks channel
of the baking Slack (and possibly other places).

# Release

See `HOWTO-release-tezos.org` if you wish to release the branch.
It is advised to wait for the test network to be live before doing that,
to catch mistakes before releasing.
