When a protocol P of Mainnet is replaced by its successor, we "freeze" P.
Freezing means removing everything which is no longer needed: delegates,
read-write operations, and tests.

# Checklist

- [ ] remove read-write client commands
- [ ] remove read-write RPCs
- [ ] remove protocol tests (in `lib_protocol/test`)
- [ ] remove client tests (in `lib_client/test`)
- [ ] remove the baker (in `bin_baker`)
- [ ] remove the endorser (in `bin_endorser`)
- [ ] remove the accuser (in `bin_accuser`)
- [ ] remove the mempool filters (in `lib_mempool` or `lib_plugin`)
- [ ] update `dune` and `.opam` files that use libraries which were removed
